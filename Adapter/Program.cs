﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<string>();
            list.Add("text1");
            list.Add("text2");
            list.Add("text3");
            list.Add("text4");

            PrinterAdapter printerAdapter = new PrinterAdapter();
            printerAdapter.Print(list);
            Console.ReadLine();
        }
    }
}
