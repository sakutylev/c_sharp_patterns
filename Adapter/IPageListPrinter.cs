﻿using System.Collections.Generic;

namespace Adapter
{
    public interface IPageListPrinter
    {
        void Print(List<string> list);
    }
}