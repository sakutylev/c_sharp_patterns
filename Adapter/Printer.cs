﻿using System;

namespace Adapter
{
    public class Printer
    {
        public void Print(String text)
        {
            Console.WriteLine(text);
        }
    }
}
