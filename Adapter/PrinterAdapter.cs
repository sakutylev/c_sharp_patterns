﻿using System;
using System.Collections.Generic;

namespace Adapter
{
    public class PrinterAdapter : IPageListPrinter
    {
        private Printer printer = new Printer();

        public void Print(List<string> list)
        {
            foreach (String text in list)
            {
                printer.Print(text);
            }
        }
    }
}
