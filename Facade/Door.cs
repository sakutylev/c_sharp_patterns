﻿using System;

namespace Facade
{
    public class Door
    {
        public void Open()
        {
            Console.WriteLine("Door open");
        }
    }
}