﻿namespace Facade
{
    public class CarFacade
    {
        private Door door = new Door();
        private Zazhiganie zazhiganie = new Zazhiganie();
        private Wheel wheel = new Wheel();

        public void Go()
        {
            door.Open();
            zazhiganie.Fire();
            wheel.Turn();
        }
    }
}