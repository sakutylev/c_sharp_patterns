﻿using System;

namespace Facade
{
    public class Wheel
    {
        public void Turn()
        {
            Console.WriteLine("Wheel turning");
        }
    }
}