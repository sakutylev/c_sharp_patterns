﻿using System;

namespace Facade
{
    public class Zazhiganie
    {
        public void Fire()
        {
            Console.WriteLine("Zazhiganie fired");
        }
    }
}