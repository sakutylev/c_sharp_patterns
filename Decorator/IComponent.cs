﻿namespace Decorator
{
    public interface IComponent
    {
        void Draw();
    }
}