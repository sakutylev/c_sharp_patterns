﻿using System;

namespace Decorator
{
    public class Window : IComponent
    {
        public void Draw()
        {
            Console.WriteLine("Window Drawing...");
        }
    }
}