﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            IComponent window = new ColorDecorator(new BorderDecorator(new Window()));
            window.Draw();


            IComponent textView = new ColorDecorator(new TextView());
            textView.Draw();
            Console.ReadLine();


        }
    }
}
