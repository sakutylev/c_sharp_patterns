﻿using System;

namespace Decorator
{
    public class ColorDecorator : Decorator
    {
        public ColorDecorator(IComponent component) : base(component)
        {
            Console.WriteLine("super(component)");
        }

        public override void AfterDraw()
        {
            Console.WriteLine("... added color");
        }
    }
}