﻿using System;

namespace Decorator
{
    public class BorderDecorator : Decorator
    {
        public BorderDecorator(IComponent component) : base(component)
        {
            Console.WriteLine("super(component)");
        }

        public override void AfterDraw()
        {
            Console.WriteLine("... added border");
        }
    }
}