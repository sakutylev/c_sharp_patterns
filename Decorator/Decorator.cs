﻿namespace Decorator
{
    public abstract class Decorator : IComponent
    {
        protected IComponent component;

        public Decorator(IComponent component)
        {
            this.component = component;
        }

        public abstract void AfterDraw();

        public void Draw()
        {
            component.Draw();
            AfterDraw();
        }
    }
}