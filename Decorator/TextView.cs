﻿using System;

namespace Decorator
{
    public class TextView : IComponent
    {
        public void Draw()
        {
            Console.WriteLine("TextView Drawing...");
        }
    }
}