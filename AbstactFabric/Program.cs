﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstactFabric
{
    class Program
    {
        private static ITransportFactory factory;
        private static String language = "EN"; 

        static void Main(string[] args)
        {
            if (language == "RU")
            {
                factory = new RussianFactory();
            }
            else
            {
                factory = new UsaFactory();
            }

            var airCraft = factory.CreaAirCraft();
            airCraft.Flight();

            var car = factory.CreateCar();
            car.Drive();
            car.Stop();

            Console.ReadLine();

        }
    }
}
