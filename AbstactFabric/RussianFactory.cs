﻿using System;

namespace AbstactFabric
{
    public class RussianFactory : ITransportFactory
    {
        public IAirCraft CreaAirCraft()
        {
            return new TU134();
        }

        public ICar CreateCar()
        {
            return new Niva();
        }
    }
}