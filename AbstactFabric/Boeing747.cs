﻿using System;

namespace AbstactFabric
{
    public class Boeing747 : IAirCraft
    {
        public void Flight()
        {
            Console.WriteLine("Boeing 747 flight!");
        }
    }
}