﻿namespace AbstactFabric
{
    public interface ICar
    {
        void Drive();
        void Stop();
    }
}
