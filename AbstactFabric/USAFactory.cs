﻿namespace AbstactFabric
{
    public class UsaFactory : ITransportFactory
    {
        public ICar CreateCar()
        {
            return new Porsche();
        }

        public IAirCraft CreaAirCraft()
        {
            return new Boeing747();
        }
    }
}