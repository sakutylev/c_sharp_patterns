﻿namespace AbstactFabric
{
    public interface ITransportFactory
    {
        ICar CreateCar();
        IAirCraft CreaAirCraft();
    }
}