﻿using System;

namespace AbstactFabric
{
    public class Niva : ICar
    {
        public void Drive()
        {
            Console.WriteLine("Niva drive");
        }

        public void Stop()
        {
            Console.WriteLine("Niva stopped");
        }
    }
}