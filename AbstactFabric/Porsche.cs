﻿using System;

namespace AbstactFabric
{
    public class Porsche : ICar
    {
        public void Drive()
        {
            Console.WriteLine("Porsche drive speed 150 km/h");
        }

        public void Stop()
        {
            Console.WriteLine("Porsche stoped at 1 sec");
        }
    }
}