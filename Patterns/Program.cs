﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world");
            var s = Singleton.GetInstance;
            Console.WriteLine(s.SayMyName());
            var s1 = Singleton.GetInstance;
            Console.WriteLine(s1.SayMyName());
            Console.ReadLine();
        }
    }

    internal class Singleton
    {
        private static Singleton _instance;

        private Singleton()
        {
            Console.WriteLine("Singleton Was created!");
        }

        public static Singleton GetInstance => _instance ?? (_instance = new Singleton());

        public string SayMyName()
        {
            return "Say my Name Bitch!";
        } 
    }
}
