﻿namespace FabricMethod
{
    public interface ICar
    {
        void Drive();
        void Stop();
    }
}
