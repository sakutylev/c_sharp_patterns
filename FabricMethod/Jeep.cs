﻿using System;

namespace FabricMethod
{
    public class Jeep : ICar
    {
        void ICar.Drive()
        {
            Console.WriteLine("Drive speed 50 km/h");
        }

        void ICar.Stop()
        {
            Console.WriteLine("Stopped at 5 sec");
        }
    }
}
