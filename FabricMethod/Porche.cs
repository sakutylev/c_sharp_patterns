﻿using System;

namespace FabricMethod
{
    public class Porche : ICar
    {
        public void Drive()
        {
            Console.WriteLine("Drive speed 150 km/h");
        }

        public void Stop()
        {
            Console.WriteLine("Stoped at 1 sec");
        }
    }
}