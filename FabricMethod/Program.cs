﻿using System;

namespace FabricMethod
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var carSelector = new CarSelector();
            var car = carSelector.GetCar(RoadType.City);
            car.Drive();
            car.Stop();

            car = carSelector.GetCar(RoadType.OffRoad);
            car.Drive();
            car.Stop();

            car = carSelector.GetCar(RoadType.Gazon);
            car.Drive();
            car.Stop();
            car.NewFunction();


            Console.ReadLine();
        }
    }
}