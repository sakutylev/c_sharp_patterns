﻿namespace FabricMethod
{
    public class CarSelector
    {
        public ICar GetCar(RoadType roadType)
        {
            ICar car = null;
            switch (roadType)
            {
                case RoadType.City:
                    car = new Porche();
                    break;
                case RoadType.OffRoad:
                    car = new Jeep();
                    break;
                case RoadType.Gazon:
                    car = new NewJeep();
                    break;
            }
            return car;
        }
        
    }
}